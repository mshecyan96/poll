create table poll.votes
(
    id        int auto_increment
        primary key,
    poll_id   int null,
    user_id   int null,
    option_id int null,
    constraint votes_uk
        unique (user_id, poll_id)
);

