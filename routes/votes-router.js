const express = require('express');
const { body } = require('express-validator');
const votesController = require('../controllers/votes-controller');

const votesRouter = express.Router();

const pollIdValidation = body('pollId').isNumeric();
const optionIdValidation = body('optionId').isNumeric();

votesRouter.post('/', [optionIdValidation, pollIdValidation], votesController.setVote);

module.exports = votesRouter;
