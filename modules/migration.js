const fs = require('fs');
const Logger = require('./logger');

const logger = new Logger('Migration.Base');
const migrationsDirname = `${__dirname}/sql`;
let migratedInProcess = false;

class Migration {
  constructor(sqlFileName) {
    this.migrationName = sqlFileName;
    this.sql = fs.readFileSync(`${migrationsDirname}/${sqlFileName}.sql`, { encoding: 'utf8' });
    this.setMigration = `INSERT IGNORE INTO migrations(name) VALUES('${sqlFileName}');`;
  }

  async up(connection) {
    try {
      if (migratedInProcess !== true) {
        const sql = fs.readFileSync(`${migrationsDirname}/migrations.sql`, { encoding: 'utf8' });
        await connection.query(sql);
        logger.info('migrations table are created successfully');
        migratedInProcess = true;
      }

      const [migrationChecker] = await connection.query(this.setMigration);

      if (Number(migrationChecker.insertId) !== 0) {
        await connection.query(this.sql);
        logger.info(`migration named ${this.migrationName} is created successfully`);
      } else {
        logger.info(`migration named ${this.migrationName} exists`);
      }

      return true;
    } catch (e) {
      logger.error('migration.module.up', e);
      return false;
    }
  }
}

module.exports = Migration;
