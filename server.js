require('dotenv').config();
const http = require('http');
const app = require('./app');
const Logger = require('./modules/logger');
const mysql = require('./modules/mysql-module');
const Migration = require('./modules/migration');

const logger = new Logger('server');
const server = http.createServer(app);
const options = {
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_POST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  multipleStatements: true,
};

mysql.connect(options)
  .then((con) => Promise.all([
    new Migration('create-users').up(con),
    new Migration('create-polls').up(con),
    new Migration('create-options').up(con),
    new Migration('create-votes').up(con),
    new Migration('create-comments').up(con),
  ]))
  .then(() => {
    server.listen(process.env.PORT, () => logger.info(`server run on port ${process.env.PORT}`));
  })
  .catch((e) => logger.error('server.start', e));
