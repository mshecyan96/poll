const winston = require('winston');

const myFormat = () => new Date(Date.now()).toUTCString();

class LoggerService {
  constructor(banner) {
    this.logData = null;
    this.banner = banner;

    const logger = winston.createLogger({
      levels: winston.config.syslog.levels,
      transports: [
        new winston.transports.Console({ level: 'debug' }),
        new winston.transports.File({
          filename: `./logs/${banner}.log`,
        }),
      ],
      format: winston.format.printf((info) => {
        let message = `[${myFormat()}] | ${info.level.toUpperCase()} | ${banner}.log | ${info.message} | `;
        message = info.obj ? `${message}data:${JSON.stringify(info.obj)} | ` : message;
        message = this.log_data ? `${message}log_data:${JSON.stringify(this.log_data)} | ` : message;
        return message;
      }),
    });
    this.logger = logger;
  }

  setLogData(logData) {
    this.log_data = logData;
  }

  info(message, obj) {
    this.logger.log('info', message, { obj });
  }

  debug(message, obj) {
    this.logger.log('debug', message, { obj });
  }

  error(message, logObj) {
    let obj = {};
    if (logObj instanceof Error) {
      obj.dependency = logObj.stack.toString().slice(0, 280);
    } else {
      obj = logObj;
    }
    this.logger.log('error', message, { obj });
  }

  warn(message, obj) {
    this.logger.log('error', message, { obj });
  }
}

module.exports = LoggerService;
