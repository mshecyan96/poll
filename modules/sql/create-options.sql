create table poll.options
(
    id      int auto_increment
        primary key,
    poll_id int         not null,
    name    varchar(50) not null,
    constraint same_option_on_single_poll
        unique (poll_id, name)
);

