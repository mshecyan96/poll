create table poll.users
(
    id         int auto_increment,
    username   varchar(150)                        not null,
    email      varchar(150)                        not null,
    avatar     varchar(150)                        null,
    created_at timestamp default CURRENT_TIMESTAMP null,
    password   varchar(150)                        not null,
    constraint table_name_email_uindex
        unique (email),
    constraint table_name_id_uindex
        unique (id)
);

alter table poll.users
    add primary key (id);

