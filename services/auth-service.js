const userModel = require('../models/user-model');
const redisClient = require('../modules/redis-module');

const authService = {
  async authorize(user) {
    const token = await redisClient.setUser(user);
    const authUser = {
      ...user,
      token,
    };

    return authUser;
  },

  async register(user) {
    const sqlSetUser = await userModel.setUser(user);
    if (Number(sqlSetUser.insertId) > 0) {
      const userToAuthorize = {
        id: sqlSetUser.insertId,
        username: user.username,
        email: user.email,
        avatar: '',
      };
      const authUser = await this.authorize(userToAuthorize);

      return authUser;
    }
    return null;
  },

  async sighIn(credentials) {
    const user = await userModel.getUser(credentials);
    if (user) {
      const userToAuthorize = {
        id: user.id,
        username: user.username,
        email: user.email,
        avatar: user.avatar,
      };
      const authUser = await this.authorize(userToAuthorize);

      return authUser;
    }
    return null;
  },

  async getUser(token) {
    const user = await redisClient.getUser(token);
    return user;
  },
};

module.exports = authService;
