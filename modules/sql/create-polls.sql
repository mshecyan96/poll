create table poll.polls
(
    id          int auto_increment
        primary key,
    user_id     int                                 not null,
    name        varchar(50)                         not null,
    description varchar(150)                        not null,
    created_at  timestamp default CURRENT_TIMESTAMP null,
    public      tinyint(1)                          null
);