const asyncRedis = require('async-redis');
const crypto = require('crypto');
const Logger = require('./logger');

const logger = new Logger('redis.module');

const client = asyncRedis.createClient();

client.on('error', (err) => {
  logger.error('redis connection error', err);
});

const storage = {
  async setUser(user) {
    try {
      const data = JSON.stringify(user);
      const random = Math.random().toString();
      const token = crypto.createHash('md5').update(random).digest('hex');
      await client.set(token, data);

      return token;
    } catch (e) {
      logger.error('setUser.method', e);
      return null;
    }
  },

  async getUser(token) {
    try {
      const dataJson = await client.get(token);
      const user = JSON.parse(dataJson);

      return user;
    } catch (e) {
      logger.error('setUser.method', e);
      return null;
    }
  },
};

module.exports = storage;
