// const { client } = require('../modules/mysql-module');
const Logger = require('../modules/logger');

const logger = new Logger('option.module');

const optionModel = {
  async getOption(data) {
    try {
      const fields = Object.keys(data);
      const values = Object.values(data);
      const condition = fields.map((field) => `${field}=?`).join(' AND ');
      const [result] = await global.mysqlClient.query(`SELECT * FROM options WHERE ${condition}`, values);

      return result[0];
    } catch (e) {
      logger.error('getOption.method', e);
      return null;
    }
  },
  async setOptions(options) {
    try {
      const subQueryArray = [];
      const dataToSet = [];

      options.forEach((option) => {
        subQueryArray.push(' (?, ?)');
        // console.log(option)
        dataToSet.push(option.poll_id.toString());
        dataToSet.push(option.name);
      });
      const subQuery = subQueryArray.join(',');
      const [result] = await global.mysqlClient.query(`INSERT IGNORE INTO options(poll_id, name) VALUES${subQuery}`, dataToSet);

      return result;
    } catch (e) {
      logger.error('setOption.method', e);
      return null;
    }
  },
};

module.exports = optionModel;
