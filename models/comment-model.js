const Logger = require('../modules/logger');

const logger = new Logger('comment.module');

const commentModel = {
  async setComment(data) {
    try {
      const setData = [data.userId, data.pollId, data.text];
      const [result] = await global.mysqlClient.query('INSERT IGNORE INTO comments(user_id, poll_id, text)  VALUES (?, ?, ?)', setData);

      return result;
    } catch (e) {
      logger.error('setComment.method', e);
      return null;
    }
  },
  async getLastComment(pollId) {
    try {
      const [result] = await global.mysqlClient.query('SELECT * FROM comments WHERE poll_id=? ORDER BY created_at DESC LIMIT 1', [pollId]);

      return result;
    } catch (e) {
      logger.error('getComment.method', e);
      return null;
    }
  },
  async getComments(pollId) {
    try {
      const [result] = await global.mysqlClient.query('SELECT * FROM comments WHERE poll_id=? ORDER BY created_at DESC', [pollId]);

      return result;
    } catch (e) {
      logger.error('getComment.method', e);
      return null;
    }
  },
};

module.exports = commentModel;
