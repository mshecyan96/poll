// const { client } = require('../modules/mysql-module');
const Logger = require('../modules/logger');

const logger = new Logger('user.module');

const userModel = {
  async getUser(data) {
    try {
      const fields = Object.keys(data);
      const values = Object.values(data);
      const condition = fields.map((field) => `${field}=?`).join(' AND ');
      const [result] = await global.mysqlClient.query(`SELECT * FROM users WHERE ${condition}`, values);

      return result[0];
    } catch (e) {
      logger.error('get.method', e);
      return null;
    }
  },
  async setUser(data) {
    try {
      const setData = [data.username, data.email, data.password];
      const [result] = await global.mysqlClient.query('INSERT IGNORE INTO users(username, email, password)  VALUES (?, ?, ?)', setData);

      return result;
    } catch (e) {
      logger.error('setUser.method', e);
      return null;
    }
  },
  async setAvatar(filename, userId) {
    try {
      const [result] = await global.mysqlClient.query('UPDATE users SET avatar=? WHERE id=?;', [filename, userId]);

      return result;
    } catch (e) {
      logger.error('setAvatar.method', e);
      return null;
    }
  },
  async updateUsersPass(email, newPass) {
    try {
      const [result] = await global.mysqlClient.query('UPDATE users SET password=? WHERE id=?;', [newPass, email]);

      return result;
    } catch (e) {
      logger.error('updateUsersPass.method', e);
      return null;
    }
  },
};

module.exports = userModel;
