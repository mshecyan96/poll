const express = require('express');
const { body } = require('express-validator');
const authMiddleware = require('../middleware/auth-middleware');
const commentsController = require('../controllers/comment-controller');

const commentsRouter = express();
const textValidation = body('text').isLength({ max: 200, min: 2 });
const pollValidation = body('pollId').isNumeric();

const commentValidator = {
  create: [textValidation, pollValidation],
};

commentsRouter.post('/', authMiddleware, commentValidator.create, commentsController.setComment);

module.exports = commentsRouter;
