const express = require('express');
const { body } = require('express-validator');
const authController = require('../controllers/auth-controller');

const authRouter = express.Router();
const usernameValidation = body('username').isLength({ min: 4, max: 12 }).withMessage('Username must contain min 4 and max 12 involves');
const emailValidation = body('email').isEmail().withMessage('Please input valid email');
const passwordValidation = body('password').isLength({ min: 6, max: 12 }).withMessage('Username must contain min 6 and max 12 involves');

const reqValidations = {
  registration: [usernameValidation, passwordValidation, emailValidation],
  login: [emailValidation, passwordValidation],
  reset: [emailValidation],
};

authRouter.post('/registration', reqValidations.registration, authController.registration);
authRouter.post('/login', reqValidations.login, authController.login);
authRouter.post('/reset', reqValidations.reset, authController.reset);
authRouter.get('/reset/:token', authController.resetPass);

module.exports = authRouter;
