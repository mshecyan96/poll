const express = require('express');
const { body, param } = require('express-validator');
const pollsController = require('../controllers/polls-controller');

const pollsRouter = express.Router();
const optionsCollectorMiddleware = (req, res, next) => {
  if (!Array.isArray(req.body.options)) {
    req.body.options = null;
  } else {
    req.body.options = req.body.options.filter((v, i) => req.body.options.indexOf(v) === i);
  }

  next();
};

const idValidation = param('id').isNumeric();
const nameValidation = body('name').isLength({ min: 2, max: 25 }).withMessage('Poll Name must contain min 2 and max 25 involves');
const descriptionValidation = body('description').isLength({ min: 10, max: 150 }).withMessage('Poll Name must contain min 10 and max 150 involves');
const optionsValidation = body('options').custom((value) => {
  // eslint-disable-next-line max-len
  if (!(Array.isArray(value) && value.length > 1 && value.length < 6) || (value.length === 2 && value[0] === value[1])) {
    throw new Error('Poll options should not be less than two y greater than 5 different values');
  }

  return true;
});
const reqValidations = {
  create: [nameValidation, descriptionValidation, optionsValidation],
};

pollsRouter.post('/', optionsCollectorMiddleware, reqValidations.create, pollsController.createPoll);
pollsRouter.route('/:id', idValidation)
  .get(pollsController.getPoll)
  .delete(pollsController.deletePoll);
pollsRouter.get('/all/:last', pollsController.getPolls);

module.exports = pollsRouter;
