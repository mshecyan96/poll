const { validationResult } = require('express-validator');
const commentModel = require('../models/comment-model');

const commentsController = {
  async setComment(req, res) {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);

    const { user } = req.body;

    if (!user) return res.status(401).send({ msg: 'unauthorized' });

    const commentData = {
      userId: user.id,
      pollId: req.body.pollId,
      text: req.body.text,
    };
    const setComment = await commentModel.setComment(commentData);

    if (setComment) return res.sendStatus(201);

    return res.status(501).send('something went wrong');
  },
};

module.exports = commentsController;
