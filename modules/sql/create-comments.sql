create table poll.comments
(
    id         int auto_increment
        primary key,
    poll_id    int                                 not null,
    user_id    int                                 null,
    created_at timestamp default CURRENT_TIMESTAMP null,
    text       varchar(255)                        not null
);

