// const { client } = require('../modules/mysql-module');
const Logger = require('../modules/logger');

const logger = new Logger('vote.module');

const voteModel = {
  async setVote(data) {
    try {
      const setData = [data.userId, data.pollId, data.optionId];
      const [result] = await global.mysqlClient.query('INSERT IGNORE INTO votes(user_id, poll_id, option_id)  VALUES (?, ?, ?)', setData);

      return result;
    } catch (e) {
      logger.error('setVote.method', e);
      return null;
    }
  },
};

module.exports = voteModel;
