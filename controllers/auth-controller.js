const { validationResult } = require('express-validator');
const crypto = require('crypto');
const authService = require('../services/auth-service');
const mailer = require('../services/mail-service');
const userModel = require('../models/user-model');

const authController = {
  async registration(req, res) {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);

    const user = {
      username: req.body.username,
      email: req.body.email,
      password: crypto.createHash('md5').update(req.body.password).digest('hex'),
    };

    const authUser = await authService.register(user);

    if (!authUser) {
      return res.status(422).send([{
        msg: 'User with same email already exits',
        param: 'email',
        location: 'body',
      }]);
    }

    return res.status(201).send({ authUser });
  },

  async login(req, res) {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);

    const credentials = {
      email: req.body.email,
      password: crypto.createHash('md5').update(req.body.password).digest('hex'),
    };
    const authUser = await authService.sighIn(credentials);

    if (!authUser) {
      return res.status(401).send([{
        msg: 'unauthorized',
      }]);
    }

    return res.status(200).send({ authUser });
  },

  async reset(req, res) {
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);
    const token = await authService.authorize({ email: req.body.email });

    return mailer(token)
      .then(() => res.cookie('reset', token, { maxAge: 600000, httpOnly: true }).send('ok'))
      .catch(() => res.sendStatus(501));
  },

  async resetPass(req, res) {
    const user = await authService.getUser(req.param.token);
    const newPass = Math.floor(Math.random() * 1000000000);

    const userUpdate = await userModel.updateUsersPass(user.email, newPass);

    if (userUpdate.insertId) return res.send(`your new pass is ${newPass}`);
    return res.sendStatus(501);
  },
};

module.exports = authController;
