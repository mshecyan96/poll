const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const mainRouter = require('./routes/main-router');

const app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(mainRouter);

module.exports = app;
