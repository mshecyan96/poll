const redisClient = require('../modules/redis-module');

module.exports = async (req, res, next) => {
  const bearer = req.headers.authorization;
  if (!bearer) {
    req.body.user = null;
  } else {
    const token = bearer.slice(7);
    const user = await redisClient.getUser(token);

    req.body.user = user;
  }
  next();
};
