const express = require('express');
const multer = require('multer');
const usersController = require('../controllers/user-controller');
const authMiddleware = require('../middleware/auth-middleware');

const uploadMiddleware = multer({ dest: 'static/uploads/' });

const userRouter = express();

userRouter.put('/', uploadMiddleware.single('avatar'), authMiddleware, usersController.setAvatar);

module.exports = userRouter;
