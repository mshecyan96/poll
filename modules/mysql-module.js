const mysql = require('mysql2/promise');
const Logger = require('./logger');

const logger = new Logger('MySql.module');
let client;

const connect = async (options) => {
  try {
    client = await mysql.createConnection(options);
    global.mysqlClient = client;
    logger.info('MySQL is connected');
    return client;
  } catch (e) {
    logger.error('connect', e);
    return process.exit;
  }
};

module.exports = {
  connect,
  // client,
};
