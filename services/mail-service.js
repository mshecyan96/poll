const nodemailer = require('nodemailer');
const Logger = require('../modules/logger');

const logger = new Logger('mailer.service');

module.exports = (token) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.MAIL,
      pass: process.env.MAIL_PASSWORD,
    },
  });

  const mailOptions = {
    from: 'pifko.app.test@gmail.com',
    to: 'mher.mshecyan@gmail.com',
    subject: 'For reset password please click on link',
    text: `${process.env.PORT}/auth/reset/${token}`,
  };
  return new Promise((res, rej) => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        rej(error);
        logger.info('Email sent', error);
      } else {
        res(info);
        logger.info('Email sent:', info.response);
      }
    });
  });
};
