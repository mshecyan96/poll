const { validationResult } = require('express-validator');
const optionModel = require('../models/option-model');
const voteModel = require('../models/vote-model');

const votesController = {
  async setVote(req, res) {
    const validationErrors = validationResult(req);
    const { user } = req.body;
    if (!validationErrors.isEmpty()) return res.status(400).send('all params must be a numeric');
    if (!user) return res.status(401).send({ msg: 'unauthorized' });

    const vote = {
      pollId: req.body.pollId,
      userId: user.id,
      optionId: req.body.optionId,
    };
    const option = await optionModel.getOption({ id: vote.optionId, poll_id: vote.pollId });

    if (!option) return res.status(400).send('Poll hav`t option');

    const setResult = await voteModel.setVote(vote);

    if (setResult.insertId > 0) return res.res.sendStatus(201);
    return res.status(400).send('Duplicate action');
  },
};

module.exports = votesController;
