const pollModel = require('../models/poll-model');
const commentModel = require('../models/comment-model');

const pollCollectorService = {
  async getPoolWithData(poll, user) {
    const usersVote = await pollModel.PollsUserVote(poll.id, user.id);
    const resultPoll = { ...poll };
    if (usersVote) {
      resultPoll.results = await pollModel.getPollResults(poll.id);
    } else {
      resultPoll.options = await pollModel.getPollOptions(poll.id);
    }
    resultPoll.lastComment = await commentModel.getLastComment(poll.id);
    return resultPoll;
  },

  async getPoolDetails(poll, user) {
    // const pollWithoutUserComment = await this.getPoolWithData;
    const usersVote = await pollModel.PollsUserVote(poll.id, user.id);
    const resultPoll = { ...poll };
    if (usersVote) {
      if (poll.public) {
        resultPoll.results = await pollModel.getPollVotes(poll.id);
      } else {
        resultPoll.results = await pollModel.getPollResults(poll.id);
      }
    } else {
      resultPoll.options = await pollModel.getPollOptions(poll.id);
    }
    resultPoll.comments = await commentModel.getComments(poll.id);
    return resultPoll;
  },
  async getOptionVotesUsers(poll, user) {
    const count = await pollModel.PollsUserVote(poll.id, user.id);
    const resultPoll = { ...poll };
    if (count) {
      resultPoll.result = await pollModel.getPollResult(poll.id);
    } else {
      resultPoll.options = await await pollModel.getPollOptions(poll.id);
    }

    return resultPoll;
  },
};

module.exports = pollCollectorService;
