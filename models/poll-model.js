// const { client } = require('../modules/mysql-module');
const Logger = require('../modules/logger');

const logger = new Logger('poll.module');

const pollModel = {
  async getPolls(last) {
    try {
      const [result] = await global.mysqlClient.query(`
        SELECT polls.id, polls.name, polls.description, polls.created_at, polls.public
        FROM polls WHERE id>? LIMIT 10`, [last]);

      return result;
    } catch (e) {
      logger.error('getPoll.method', e);
      return null;
    }
  },
  async getPoll(id) {
    try {
      const [result] = await global.mysqlClient.query(`
        SELECT polls.id, polls.name, polls.description, polls.created_at, polls.public
        FROM polls WHERE id=?`, [id]);

      return result[0];
    } catch (e) {
      logger.error('getPoll.method', e);
      return null;
    }
  },
  async getPollsVotes(pollId, userId) {
    try {
      const result = await global.mysqlClient.query('SELECT COUNT(id) count FROM votes WHERE poll_id=? AND user_id=?', [pollId, userId]);
      return result;
    } catch (e) {
      logger.error('VoteFromUser.method', e);
      return null;
    }
  },
  async PollsUserVote(pollId, userId) {
    try {
      const [result] = await global.mysqlClient.query('SELECT COUNT(id) count FROM votes WHERE poll_id=? AND user_id=?', [pollId, userId]);
      // console.log(result)
      return result[0].count;
    } catch (e) {
      logger.error('VoteFromUser.method', e);
      return null;
    }
  },
  async getPollResults(pollId) {
    try {
      const [result] = await global.mysqlClient.query(`
      SELECT options.id, options.name, COUNT(votes.id) AS count
      FROM options
      LEFT JOIN votes ON options.id = votes.option_id
      WHERE options.poll_id=?
      GROUP BY options.id
      `, [pollId]);

      return result;
    } catch (e) {
      logger.error('getOption.method', e);
      return null;
    }
  },
  async getPollVotes(pollId) {
    try {
      const [result] = await global.mysqlClient.query(`
      SELECT options.id option_id, options.name option_name, votes.id vote_count, votes.user_id user_id, u.username username
      FROM options
      LEFT JOIN votes ON options.id = votes.option_id
      LEFT JOIN users u on votes.user_id = u.id
      WHERE options.poll_id=?
      `, [pollId]);

      return result;
    } catch (e) {
      logger.error('getOption.method', e);
      return null;
    }
  },
  async getPollOptions(pollId) {
    try {
      const [result] = await global.mysqlClient.query(`
      SELECT options.id, options.name
      FROM options
      WHERE options.poll_id=?
      GROUP BY options.id
      `, [pollId]);

      return result;
    } catch (e) {
      logger.error('getOption.method', e);
      return null;
    }
  },
  async setPoll(data) {
    try {
      const setData = [data.userId, data.name, data.description];
      const [result] = await global.mysqlClient.query('INSERT IGNORE INTO polls(user_id, name, description)  VALUES (?, ?, ?)', setData);

      return result;
    } catch (e) {
      logger.error('setPoll.method', e);
      return null;
    }
  },
  async deletePoll(id, userId) {
    try {
      const [result] = await global.mysqlClient.query('DELETE FROM polls WHERE id=? AND user_id=?', [id, userId]);

      return result.affectedRows;
    } catch (e) {
      logger.error('setPoll.method', e);
      return null;
    }
  },
};

module.exports = pollModel;
