const userModel = require('../models/user-model');

const usersController = {
  async setAvatar(req, res) {
    const { user } = req.body;
    const { file } = req;

    if (!file) {
      return res.status(422).send([{
        msg: 'File is required',
        param: 'avatar',
        location: 'body',
      }]);
    }
    if (!user) return res.status(401).send('unauthorized');

    const setAvatarResult = await userModel.setAvatar(file.filename, user.id);

    if (setAvatarResult.insertId) return res.sendStatus(200);

    return res.status(501).send('something went wrong');
  },
};

module.exports = usersController;
