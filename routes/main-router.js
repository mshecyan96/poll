const express = require('express');
const authRouter = require('./auth-router');
const pollsRouter = require('./polls-router');
const votesRouter = require('./votes-router');
const userRouter = require('./user-router');
const commentsRouter = require('./comments-router');

const mainRouter = express.Router();
const authMiddleware = require('../middleware/auth-middleware');

mainRouter.use('/auth', authRouter);
mainRouter.use('/user', userRouter);
mainRouter.use('/polls', authMiddleware, pollsRouter);
mainRouter.use('/vote', authMiddleware, votesRouter);
mainRouter.use('/comment', commentsRouter);

module.exports = mainRouter;
