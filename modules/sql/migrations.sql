create table if not exists poll.migrations
(
    id          int auto_increment
        primary key,
    name        varchar(150)                        not null,
    migrated_at timestamp default CURRENT_TIMESTAMP null,
    constraint migrationg_name_uindex
        unique (name)
);
