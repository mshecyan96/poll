const { validationResult } = require('express-validator');
const optionModel = require('../models/option-model');
const pollModel = require('../models/poll-model');
const pollCollectorService = require('../services/poll-corrector-service');

const pollsController = {
  async createPoll(req, res) {
    const validationErrors = validationResult(req);
    const { user } = req.body;
    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);
    if (!user) return res.status(401).send({ msg: 'unauthorized' });

    const newPoll = {
      name: req.body.name,
      description: req.body.description,
      userId: req.body.user.id,
    };
    const newPollResult = await pollModel.setPoll(newPoll);
    const newPollId = newPollResult.insertId;

    if (!newPollId) return res.status(501).send({ msg: 'Something want wrong' });

    const newOptions = req.body.options.map((option) => ({
      name: option,
      poll_id: newPollId,
    }));
    const newOptionsResult = await optionModel.setOptions(newOptions);

    if (!newOptionsResult) return res.status(501).send({ msg: 'Something want wrong' });

    const responseData = {
      id: newPollId,
      name: newPoll.name,
      description: newPoll.description,
      options: req.body.options,
    };

    return res.status(201).send(responseData);
  },

  async getPoll(req, res) {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);

    const { user } = req.body;

    if (!user) return res.status(401).send({ msg: 'unauthorized' });

    const poll = await pollModel.getPoll(req.params.id);
    const resultPoll = await pollCollectorService.getPoolDetails(poll, user);

    return res.send(resultPoll);
  },

  async getPolls(req, res) {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) return res.status(422).send(validationErrors.errors);

    const { user } = req.body;
    const last = (Number.isNaN(req.params.last)) ? 0 : req.params.last;
    const polls = await pollModel.getPolls(last);

    if (!user) return res.send(polls);
    // eslint-disable-next-line max-len
    const resultPollsPromises = polls.map((poll) => (pollCollectorService.getPoolWithData(poll, user)));

    const resultPolls = await Promise.all(resultPollsPromises);

    return res.send(resultPolls);
  },

  async deletePoll(req, res) {
    const { user } = req.body;

    if (!user) return res.status(401).send({ msg: 'unauthorized' });

    const deleted = await pollModel.deletePoll(req.params.id, user.id);

    if (!deleted) return res.status(400).send('User don`t have permission');

    return res.sendStatus(200);
  },
};

module.exports = pollsController;
